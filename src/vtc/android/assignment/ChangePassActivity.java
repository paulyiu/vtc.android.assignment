package vtc.android.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import vtc.android.assignment.model.User;
import vtc.android.assignment.util.*;
import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Activity for change password
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class ChangePassActivity extends Activity implements OnClickListener {
	/**
	 * Use to hold all View items
	 */
	Map<String, View> view = new HashMap<String, View>();
	/**
	 * User ID of logged in user
	 */
	int uid;
	/**
	 * User name of logged in user
	 */
	String username;

	/**
	 * Call {@link Util#findViewById(Activity, ViewGroup, Map)} to map XML item to View. Retrieve {@link #uid} and {@link #username} from Intent
	 * 
	 * @see Intent
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_change_pass);
		uid = getIntent().getIntExtra("uid", -1);
		username = getIntent().getStringExtra("username");
		Tools.findViewById(this, (ViewGroup) findViewById(android.R.id.content), view);
	}

	/**
	 * Handle all click event Verify old password, confirm new password and then change password
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btSubmitChangePass:
			String oldPass = ((EditText) view.get("etOldPass")).getText().toString();
			String newPass1 = ((EditText) view.get("etNewPass1")).getText().toString();
			String newPass2 = ((EditText) view.get("etNewPass2")).getText().toString();
			try {
				if (newPass1.equals(newPass2)) {
					DbHelper db = DbHelper.getInstance(getApplicationContext());
					ArrayList<User> users = db.listUser("_id = ? and password = ?", new String[] { "" + uid, Tools.md5(oldPass) }, null, null, null);
					if (users.size() > 0) {
						User user  = new User(uid,username,Tools.md5(newPass1));
						db.updateUser(user);
						Toast.makeText(this, "Password changed", Toast.LENGTH_LONG).show();
						finish();
					} else {
						Toast.makeText(this, "Old password not match", Toast.LENGTH_LONG).show();
					}
				} else {
					Toast.makeText(this, "New Password not match", Toast.LENGTH_LONG).show();
				}
			} catch (SQLiteException e) {
				e.printStackTrace();
			} 
			break;
		}

	}
}
