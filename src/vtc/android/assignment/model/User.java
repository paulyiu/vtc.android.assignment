package vtc.android.assignment.model;

public class User {
	private int mId;
	private String mUserName, mPassword;

	public User(String username, String password) {
		this(-1,username,password);
	}

	public User(int _id, String username, String password) {
		super();
		this.mId = _id;
		this.mUserName = username;
		this.mPassword = password;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public String getmUserName() {
		return mUserName;
	}

	public void setmUserName(String mUserName) {
		this.mUserName = mUserName;
	}

	public String getmPassword() {
		return mPassword;
	}

	public void setmPassword(String mPassword) {
		this.mPassword = mPassword;
	}


}
