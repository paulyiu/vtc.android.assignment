package vtc.android.assignment.model;

public enum Gender {
	FEMALE(0), MALE(1);

	private final int value;

	Gender(int value) {
		this.value = value;
	}

	public int getInt() {
		return value;
	}
	
	public String toString(){
		switch(this){
		case FEMALE: return "Female";
		case MALE: return "Male";
		}
		return "Neuter";
	}
}
