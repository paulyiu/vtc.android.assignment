package vtc.android.assignment.model;

public class Survey {
	private int mId, mUid;
	private AgeRange mAgeRange;
	private Gender mGender;
	private Rating mRating;
	private long mCreateDate;

	public Survey(int mUid, AgeRange mAgeRange, Gender mGender, Rating mRating, long mCreateDate) {
		this(-1, mUid, mAgeRange, mGender, mRating, mCreateDate);
	}

	public Survey(int mId, int mUid, AgeRange mAgeRange, Gender mGender, Rating mRating,
			long mCreateDate) {
		super();
		this.mId = mId;
		this.mUid = mUid;
		this.mAgeRange = mAgeRange;
		this.mGender = mGender;
		this.mRating = mRating;
		this.mCreateDate = mCreateDate;
	}

	public int getmId() {
		return mId;
	}

	public void setmId(int mId) {
		this.mId = mId;
	}

	public int getmUid() {
		return mUid;
	}

	public void setmUid(int mUid) {
		this.mUid = mUid;
	}

	public AgeRange getmAgeRange() {
		return mAgeRange;
	}

	public void setmAgeRange(AgeRange mAgeRange) {
		this.mAgeRange = mAgeRange;
	}

	public Gender getmGender() {
		return mGender;
	}

	public void setmGender(Gender mGender) {
		this.mGender = mGender;
	}

	public Rating getmRating() {
		return mRating;
	}

	public void setmRating(Rating mRating) {
		this.mRating = mRating;
	}

	public long getmCreateDate() {
		return mCreateDate;
	}

	public void setmCreateDate(long mCreateDate) {
		this.mCreateDate = mCreateDate;
	}

}
