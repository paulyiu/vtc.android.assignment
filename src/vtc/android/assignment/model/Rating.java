package vtc.android.assignment.model;

public enum Rating {
	DONT_KNOW(0),
	VERY_DISSATISFIED(1),
	SOMEWHAT_DISSATISFIED(2),
	NEITHER_SATISFIED_NOR_DISSATISFIED(3),
	SOMEWHAT_SATISFIED(4),
	VERY_SATISFIED(5)
	;
	
	private final int value;
	Rating(int value){
		this.value = value;
	}
	
	public int getInt(){
		return value;
	}
	
	public String toString(){
		switch(this){
		case VERY_DISSATISFIED: return "Very dissatisfied";
		case SOMEWHAT_DISSATISFIED: return"Somewhat dissatisfied";
		case NEITHER_SATISFIED_NOR_DISSATISFIED: return "Neither satisfied nor dissatisfied";
		case SOMEWHAT_SATISFIED: return "Somewhat satisfied";
		case VERY_SATISFIED: return "Very satisfied";
		case DONT_KNOW: return "Don't know";
		}
		return "Don't know";
	}
	
}
