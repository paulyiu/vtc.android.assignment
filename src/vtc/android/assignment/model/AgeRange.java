package vtc.android.assignment.model;

public enum AgeRange {
	RANGE_0_10(0), 
	RANGE_11_20(1), 
	RANGE_21_30(2), 
	RANGE_31_40(3), 
	RANGE_41_50(4), 
	RANGE_51_60(5), 
	RANGE_OVER_60(6);

	private final int range;

	AgeRange(int range) {
		this.range = range;
	}

	public int getInt() {
		return range;
	}

	public String toString() {
		switch (this) {
		case RANGE_0_10: return "0-10";
		case RANGE_11_20: return "11-20";
		case RANGE_21_30: return "21-30";
		case RANGE_31_40: return "31-40";
		case RANGE_41_50: return "41-50";
		case RANGE_51_60: return "51-60";
		case RANGE_OVER_60: return "Over 60";
		}
		return "Out of Range";
	}
}
