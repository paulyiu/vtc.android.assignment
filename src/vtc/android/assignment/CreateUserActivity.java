package vtc.android.assignment;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import vtc.android.assignment.model.User;
import vtc.android.assignment.util.DbHelper;
import vtc.android.assignment.util.Tools;
import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Activity for create new user
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class CreateUserActivity extends Activity implements OnClickListener {
	/**
	 * Use to hold all View items
	 */
	Map<String, View> view = new HashMap<String, View>();

	/**
	 * Call {@link Util#findViewById(Activity, ViewGroup, Map)} to map XML item
	 * to View
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_createuser);

		Tools.findViewById(this, (ViewGroup) findViewById(android.R.id.content), view);
	}

	/**
	 * Handle all click event. Call {@link #addUser()} when add user button
	 * clicked and load {@link LoginActivity} after {@link #addUser()}
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btAddUser:
			if (addUser()) {
				startActivity(new Intent(this, MainActivity.class)
						.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
			}
			break;
		}

	}

	/**
	 * Create addition user
	 * 
	 * @return boolean Return true if user create success.
	 */
	private boolean addUser() {
		try {
			DbHelper db = DbHelper.getInstance(getApplicationContext());
			User user = new User(((TextView) view.get("etUserName")).getText().toString()
					.toLowerCase(Locale.US), Tools.md5(((TextView) view.get("etPassword"))
					.getText().toString()));
			if (db.insertUser(user) > -1) {
				Toast.makeText(this, "User: " + user.getmUserName() + " added",
						Toast.LENGTH_LONG).show();
				return true;
			} else {
				Toast.makeText(this,
						"Failed: User " + user.getmUserName() + " already exist",
						Toast.LENGTH_LONG).show();
				return false;
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		return false;
	}

}
