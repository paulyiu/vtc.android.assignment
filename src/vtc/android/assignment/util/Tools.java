package vtc.android.assignment.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;

public class Tools {

	/**
	 * Map XML item to View
	 * 
	 * @param activity Current activity (this)
	 * @param vg ViewGroup for mapping
	 * @param view The Collection to hold Views
	 */
	public static void findViewById(Activity activity, ViewGroup vg, Map<String, View> view) {
		String resKey;
		int count = vg.getChildCount();
		for (int i = 0; i < count; i++) {
			View v = vg.getChildAt(i);
			int resid = v.getId();

			if (v instanceof Button)
				((Button) v).setOnClickListener((OnClickListener) activity);
			if(v instanceof RadioButton)
				((RadioButton) v).setOnClickListener((OnClickListener) activity);
			if(v instanceof Spinner)
				((Spinner) v).setOnItemSelectedListener((OnItemSelectedListener) activity);
			if (v instanceof CheckBox)
				((Button) v).setOnClickListener((OnClickListener) activity);
			if (v instanceof SeekBar)
				((SeekBar) v).setOnSeekBarChangeListener((OnSeekBarChangeListener) activity);
			if (v instanceof TabHost)
				((TabHost) v).setOnTabChangedListener((OnTabChangeListener) activity);
			if (resid > -1) {
				resKey = activity.getResources().getResourceEntryName(resid);
				view.put(resKey, v);
			}
			if (v instanceof ViewGroup)
				findViewById(activity, (ViewGroup) v, view);

		}
	}

	/**
	 * Encrypt string to MD5
	 * 
	 * @param s String to encrpyt
	 * @return String Hex string
	 */
	public static String md5(String s) {
		try {
			// Create MD5 Hash
			MessageDigest digest = java.security.MessageDigest.getInstance("MD5");
			digest.update(s.getBytes());
			byte messageDigest[] = digest.digest();

			// Create Hex String
			StringBuffer hexString = new StringBuffer();
			for (int i = 0; i < messageDigest.length; i++)
				hexString.append(Integer.toHexString(0xFF & messageDigest[i]));
			return hexString.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return "";
	}
}
