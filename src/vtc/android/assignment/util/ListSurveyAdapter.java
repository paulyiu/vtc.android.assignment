package vtc.android.assignment.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import vtc.android.assignment.R;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * Adapter class for list survey data
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class ListSurveyAdapter extends BaseAdapter {

	ArrayList<HashMap<String, String>> list;
	Activity activity;

	public ListSurveyAdapter() {
	}

	/**
	 * Constructor for create adapter
	 * 
	 * @param activity Current activity (this)
	 * @param list Data to list in ListView
	 */
	public ListSurveyAdapter(Activity activity, ArrayList<HashMap<String, String>> list) {
		super();
		this.activity = activity;
		this.list = list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	/**
	 * Inner Class to hold each Row
	 * 
	 * @author Yiu Nagi Pang (111809880)
	 * @version 1.0
	 * @since 2015-03-28
	 */
	private class ViewHolder {
		/**
		 * TextView for each column
		 */
		Map<Integer, View> row;
		Map<Integer, String> header;

		/**
		 * Place holder for each row
		 * 
		 * @param map Column header and Row data
		 */
		public ViewHolder(HashMap<String, String> map) {
			row = new HashMap<Integer, View>();
			header = new HashMap<Integer, String>();
			int i = 0;
			for (String key : map.keySet()) {
				header.put(i, key);
				row.put(i++, new TextView(activity));
			}
		}
	}

	/**
	 * Covert data in HashMap to display in ListViwe
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		LayoutInflater inflater = activity.getLayoutInflater();
		HashMap<String, String> map = list.get(position);

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.listview_survey_list, null);
			holder = new ViewHolder(map);
			for (int i = 0; i < holder.header.size(); i++) {
				holder.row.put(i, (TextView) convertView.findViewById(((ViewGroup) convertView).getChildAt(i).getId()));
			}
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		for (int i = 0; i < holder.header.size(); i++) {
			((TextView) holder.row.get(i)).setText(map.get(holder.header.get(i)));
		}
		return convertView;
	}

}
