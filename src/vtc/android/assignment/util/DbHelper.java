package vtc.android.assignment.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import vtc.android.assignment.LoginActivity;
import vtc.android.assignment.model.AgeRange;
import vtc.android.assignment.model.Gender;
import vtc.android.assignment.model.Rating;
import vtc.android.assignment.model.Survey;
import vtc.android.assignment.model.User;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

/**
 * Methods to ease db related operation
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class DbHelper extends SQLiteOpenHelper {

	/**
	 * Tags for logcat
	 */
	private static final String TAG = DbHelper.class.getName();

	/**
	 * version for update database schema
	 */
	private static final int DB_VERSION = 1;

	/**
	 * database file name
	 */
	private static final String DB_NAME = "Surveys.db";

	/**
	 * Application context
	 */
	private Context mContext;
	/**
	 * all tables object
	 */
	private HashMap<String, LinkedHashMap<Integer, String[]>> mTables;

	/**
	 * helper object for singleton
	 */
	private static DbHelper sInstance;

	/**
	 * define all tables schema
	 * 
	 * @param context
	 */
	private DbHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		String mTableName;
		LinkedHashMap<Integer, String[]> mColumn;
		mTables = new HashMap<String, LinkedHashMap<Integer, String[]>>();
		mContext = context;

		mTableName = "Users";
		mColumn = new LinkedHashMap<Integer, String[]>();
		mColumn.put(0, new String[] { "_id", "INTEGER", "PRIMARY KEY AUTOINCREMENT" });
		mColumn.put(1, new String[] { "username", "TEXT", "UNIQUE" });
		mColumn.put(2, new String[] { "password", "TEXT", "" });
		mTables.put(mTableName, mColumn);

		mTableName = "Surveys";
		mColumn = new LinkedHashMap<Integer, String[]>();
		mColumn.put(0, new String[] { "_id", "INTEGER PRIMARY KEY AUTOINCREMENT" });
		mColumn.put(1, new String[] { "uid", "INTEGER", "" });
		mColumn.put(2, new String[] { "agerange", "int", "" });
		mColumn.put(3, new String[] { "gender", "int", "" });
		mColumn.put(4, new String[] { "rating", "int", "" });
		mColumn.put(5, new String[] { "createdate", "DATETIME", "" });
		mTables.put(mTableName, mColumn);
	}

	/**
	 * DbHelper singleton
	 * 
	 * @param context
	 * @return
	 */
	public static DbHelper getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new DbHelper(context.getApplicationContext());
		}
		return sInstance;
	}

	/**
	 * create tables
	 */
	@Override
	public void onCreate(SQLiteDatabase db) {
		for (String table : mTables.keySet()) {
			LinkedHashMap<Integer, String[]> columns = mTables.get(table);
			StringBuffer sb = new StringBuffer("CREATE TABLE ");
			sb.append(table);
			sb.append("(");
			for (int key : columns.keySet()) {
				for (String col : columns.get(key)) {
					sb.append(col + " ");
				}
				sb.setLength(sb.length() - 1);
				sb.append(",");
			}
			sb.setLength(sb.length() - 1);
			sb.append(")");
			Log.i(TAG, sb.toString());
			db.execSQL(sb.toString());
		}
	}

	/**
	 * Remove all table and recreate it
	 */
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		for (String table : mTables.keySet()) {
			db.execSQL("DROP TABLE IF EXISTS " + table);
		}

		onCreate(db);
	}

	/**
	 * close db object if is open
	 * 
	 * @param db
	 */
	public static void close(SQLiteDatabase db) {
		if (db != null && db.isOpen()) {
			db.close();
		}
	}

	/**
	 * close cursor object if not null
	 * 
	 * @param cursor
	 */
	public static void close(Cursor cursor) {
		if (cursor != null) {
			cursor.close();
		}
	}

	/**
	 * Simple query method
	 * 
	 * @param table
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return Cursor
	 */
	public Cursor query(String table, String selection, String[] selectionArgs, String groupBy,
			String having, String orderBy) {
		SQLiteDatabase db = this.getReadableDatabase();
		return db.query(table, null, selection, selectionArgs, groupBy, having, orderBy);
	}

	public void querysWithTransaction(ArrayList<String> sqls) {
		SQLiteDatabase db = this.getWritableDatabase();
		try{
		db.beginTransaction();
		for (String s : sqls) {
			db.execSQL(s);
			Log.i(TAG, s);
		}
		db.setTransactionSuccessful();
		Toast.makeText(mContext, "Database restored", Toast.LENGTH_LONG).show();
		mContext.startActivity(new Intent(mContext, LoginActivity.class)
				.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
		}catch(SQLiteException e){
			e.printStackTrace();
		} finally{
			db.endTransaction();
			close(db);
		}
	}

	/**
	 * Raw query
	 * 
	 * @param sql
	 * @param selectionArgs
	 * @return
	 */
	public Cursor rawQuery(String sql, String[] selectionArgs) {
		SQLiteDatabase db = this.getReadableDatabase();
		return db.rawQuery(sql, selectionArgs);
	}

	/**
	 * Retrieve data from database and store in ArrayList
	 * 
	 * @param dbName The table name of data
	 * @return ArrayList<String[]> The ArrayList of data
	 */
	public ArrayList<String[]> dbToArrayList(String dbName) {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = null;
		ArrayList<String[]> rows = new ArrayList<String[]>();
		try {
			cursor = db.query(dbName, null, null, null, null, null, null);
			while (cursor.moveToNext()) {
				int count = cursor.getColumnCount();
				String[] temp = new String[count];
				for (int i = 0; i < count; i++) {
					temp[i] = cursor.getString(i);
				}
				rows.add(temp);
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} finally {
			close(db);
			close(cursor);
		}
		return rows;
	}

	/**
	 * Insert method for users table
	 * 
	 * @param user
	 * @return long The row id of inserted user
	 */
	public long insertUser(User user) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(mTables.get("Users").get(1)[0], user.getmUserName());
		values.put(mTables.get("Users").get(2)[0], user.getmPassword());
		long result = db.insert("Users", null, values);
		close(db);
		return result;
	}

	/**
	 * List method for users table
	 * 
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return ArrayList<User>
	 */
	public ArrayList<User> listUser(String selection, String[] selectionArgs, String groupBy,
			String having, String orderBy) {
		ArrayList<User> users = new ArrayList<User>();
		Cursor c = query("Users", selection, selectionArgs, groupBy, having, orderBy);
		if (c != null) {
			while (c.moveToNext()) {
				User user = new User(c.getInt(0), c.getString(1), c.getString(2));
				users.add(user);
			}
			close(c);
		}
		return users;
	}

	/**
	 * Update method for users table
	 * 
	 * @param user
	 * @return int
	 */
	public int updateUser(User user) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(mTables.get("Users").get(1)[0], user.getmUserName());
		values.put(mTables.get("Users").get(2)[0], user.getmPassword());
		int result = db.update("Users", values, "_id = ?",
				new String[] { String.valueOf(user.getmId()) });
		close(db);
		return result;
	}

	/**
	 * Delete method for users table
	 * 
	 * @param user
	 * @return int
	 */
	public int deleteUser(User user) {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete("Users", "_id = ?", new String[] { String.valueOf(user.getmId()) });
		close(db);
		return result;
	}

	/**
	 * Insert method for surveys table
	 * 
	 * @param survey
	 * @return long
	 */
	public long insertSurvey(Survey survey) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(mTables.get("Surveys").get(1)[0], survey.getmUid());
		values.put(mTables.get("Surveys").get(2)[0], survey.getmAgeRange().getInt());
		values.put(mTables.get("Surveys").get(3)[0], survey.getmGender().getInt());
		values.put(mTables.get("Surveys").get(4)[0], survey.getmRating().getInt());
		values.put(mTables.get("Surveys").get(5)[0], survey.getmCreateDate());
		long result = db.insert("Surveys", null, values);
		close(db);
		return result;
	}

	/**
	 * List method for surveys table
	 * 
	 * @param selection
	 * @param selectionArgs
	 * @param groupBy
	 * @param having
	 * @param orderBy
	 * @return ArrayList<Survey>
	 */
	public ArrayList<Survey> listSurvey(String selection, String[] selectionArgs, String groupBy,
			String having, String orderBy) {
		ArrayList<Survey> surveys = new ArrayList<Survey>();
		Cursor c = query("Surveys", selection, selectionArgs, groupBy, having, orderBy);
		if (c != null) {
			while (c.moveToNext()) {
				Survey survey = new Survey(c.getInt(0), c.getInt(1),
						AgeRange.values()[c.getInt(2)], Gender.values()[c.getInt(3)],
						Rating.values()[c.getInt(4)], c.getLong(5));
				surveys.add(survey);
			}
			close(c);
		}
		return surveys;
	}

	/**
	 * Update method for surveys table
	 * 
	 * @param survey
	 * @return int
	 */
	public int updateSurvey(Survey survey) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(mTables.get("Surveys").get(1)[0], survey.getmUid());
		values.put(mTables.get("Surveys").get(2)[0], survey.getmAgeRange().getInt());
		values.put(mTables.get("Surveys").get(3)[0], survey.getmGender().getInt());
		values.put(mTables.get("Surveys").get(4)[0], survey.getmRating().getInt());
		values.put(mTables.get("Surveys").get(5)[0], survey.getmCreateDate());
		int result = db.update("Surveys", values, "_id = ?",
				new String[] { String.valueOf(survey.getmId()) });
		close(db);
		return result;
	}

	/**
	 * Delete method for surveys table
	 * 
	 * @param survey
	 * @return int
	 */
	public int deleteSurvey(Survey survey) {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete("Surveys", "_id = ?",
				new String[] { String.valueOf(survey.getmId()) });
		close(db);
		return result;
	}

	/**
	 * Clean method for surveys table
	 * 
	 * @return int
	 */
	public int deleteAllSurvey() {
		SQLiteDatabase db = this.getWritableDatabase();
		int result = db.delete("Surveys", "1", null);
		if (result > 0) {
			db.delete("sqlite_sequence", "name = ?", new String[] { "Surveys" });
		}
		close(db);
		return result;
	}

}
