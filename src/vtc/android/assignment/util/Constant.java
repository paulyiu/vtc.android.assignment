package vtc.android.assignment.util;

import android.graphics.Color;
import android.os.Environment;

/**
 * Common constant variables
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class Constant {
	/**
	 * database file name {@value #DB_File}
	 */
	public final static String DB_File = "Surveys.db";

	/**
	 * Header for ListView of survey data
	 */
	public final static String[] SURVEY_LIST_HEADER = { "Survey ID", "Age Range", "Gender",
			"Rating", "Created By", "Create Date" };
	/**
	 * PATH for database export
	 */
	public final static String CSV_PATH = Environment
			.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)
			+ "/vtc.android.assignment.csv";
	public final static int[] color = {Color.YELLOW,Color.MAGENTA,Color.GREEN,Color.CYAN,Color.BLUE,Color.RED}; 
}
