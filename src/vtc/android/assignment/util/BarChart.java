package vtc.android.assignment.util;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;
/**
 * Draw Bar chart base on data
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class BarChart extends View {

	private ArrayList<HashMap<String, String>> list;
	private HashMap<String, Integer> mData = new HashMap<String, Integer>();
	private int mWidth, mHeight, mPadding, mMaxX, mMaxY, mStepX, mStepY;
	String title;
	ArrayList<String> header = new ArrayList<String>();
	Paint paint;

	public BarChart(Context context, ArrayList<HashMap<String, String>> list) {
		super(context);
		this.list = list;
		title = "  ";
		paint = new Paint();
		if (list != null && list.size() > 0) {
			mPadding = 10;
			mMaxX = list.size();
			mMaxY = 0;
			String data, gender, rating, agerange;

			for (HashMap<String, String> m : list) {
				title = "Survey statistics grouped by  ";
				data = "";
				if (m.get("Gender") != null) {
					if (m.get("Gender").equals("F")) {
						gender = "Female\n";
					} else {
						gender = "Male\n";
					}
					title += "Gender, ";
				} else {
					gender = "";
				}
				data += gender;
				if (m.get("Rating") != null) {
					rating = "Rating " + m.get("Rating") + "\n";
					title += "Rating, ";
				} else {
					rating = "";
				}
				data += rating;
				if (m.get("Age Range") != null) {
					agerange = "Age " + m.get("Age Range") + "\n";
					title += "Age Range, ";
				} else {
					agerange = "";
				}
				data += agerange;
				header.add(gender + rating + agerange);
				if (Integer.parseInt(m.get("count(*)")) > mMaxY) {
					mMaxY = Integer.parseInt(m.get("count(*)"));
				}
				mData.put(data, Integer.parseInt(m.get("count(*)")));
			}
			title = title.substring(0, title.length() - 2);

		}
	}

	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		mHeight = h;
		mWidth = w;
		if (list != null && list.size() > 0) {
			mStepX = (mWidth - (2 * mPadding) - 300) / mMaxX;
			mStepY = (mHeight - (2 * mPadding) - 300) / (mMaxY + 1);
		}
	}

	@Override
	public void onDraw(Canvas c) {
		super.onDraw(c);
		if (list != null && list.size() > 0) {
			paint.setStyle(Paint.Style.FILL);
			// make the entire canvas white
			paint.setColor(Color.TRANSPARENT);
			c.drawPaint(paint);
			paint.setColor(Color.BLACK);
			paint.setStrokeWidth(3);
			// draw the border
			c.drawLine(mPadding, mPadding, mWidth - mPadding, mPadding, paint);
			c.drawLine(mPadding, mPadding, mPadding, mHeight - mPadding - 100, paint);
			c.drawLine(mWidth - mPadding, mPadding, mWidth - mPadding, mHeight - mPadding - 100,
					paint);
			c.drawLine(mPadding, mHeight - mPadding - 100, mWidth - mPadding, mHeight - mPadding
					- 100, paint);
			// draw title
			paint.setColor(Color.BLACK);
			paint.setStyle(Paint.Style.FILL);
			paint.setTextSize(35);
			paint.setTypeface(Typeface.create("Arial", Typeface.BOLD));
			c.drawText(title, mPadding + 50, mPadding + 50, paint);
			// draw the XY line
			c.drawLine(mPadding + 100, mPadding + 100, mPadding + 100, mHeight - mPadding - 250,
					paint);
			c.drawLine(mPadding + 100, mHeight - mPadding - 250, mWidth - mPadding - 100, mHeight
					- mPadding - 250, paint);
			// draw X background line
			paint.setTextSize(20);
			for (int i = 0; i < mMaxX; i++) {
				paint.setColor(Color.GRAY);
				c.drawLine(mPadding + 200 + (mStepX * i), mPadding + 100, mPadding + 200
						+ (mStepX * i), mHeight - mPadding - 250, paint);
			}
			// draw Y background line
			for (int i = 0; i < mMaxY + 1; i++) {
				paint.setColor(Color.GRAY);
				c.drawLine(mPadding + 100, mHeight - mPadding - 250 - (mStepY * i), mWidth
						- mPadding - 100, mHeight - mPadding - 250 - (mStepY * i), paint);
				paint.setColor(Color.BLACK);
				c.drawText("" + i, mPadding + 50, mHeight - 250 - (mStepY * i), paint);
			}
			// draw the bar chart
			for (int i = 0; i < mMaxX; i++) {
				String[] h = header.get(i).split("\n");
				paint.setColor(Constant.color[i % Constant.color.length]);
				c.drawRect(mPadding + 150 + (mStepX * i), mHeight - mPadding - 250
						- (mStepY * Integer.parseInt(list.get(i).get("count(*)"))), mPadding + 200
						+ (mStepX * i) + 50, mHeight - mPadding - 250, paint);
				paint.setColor(Color.BLACK);
				for (int j = 0; j < h.length; j++) {
					c.drawText(h[j], mPadding + 150 + (mStepX * i), mHeight - 220
							+ (j * 3 * mPadding), paint);
				}
			}
		}
	}
}
