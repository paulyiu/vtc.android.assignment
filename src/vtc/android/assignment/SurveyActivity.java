package vtc.android.assignment;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;

import vtc.android.assignment.model.AgeRange;
import vtc.android.assignment.model.Gender;
import vtc.android.assignment.model.Rating;
import vtc.android.assignment.model.Survey;
import vtc.android.assignment.util.BarChart;
import vtc.android.assignment.util.Constant;
import vtc.android.assignment.util.DbHelper;
import vtc.android.assignment.util.ListSurveyAdapter;
import vtc.android.assignment.util.Tools;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TabWidget;
import android.widget.TextView;
import android.widget.Toast;

import com.opencsv.CSVWriter;

/**
 * Survey Activity for record survey data and list record, check statistics and
 * chart
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class SurveyActivity extends Activity implements OnClickListener, OnSeekBarChangeListener,
		OnTabChangeListener, OnItemSelectedListener {

	/**
	 * Use to hold all View items
	 */
	Map<String, View> view = new HashMap<String, View>();
	/**
	 * User ID of logged in user
	 */
	int uid;
	/**
	 * User name of logged in user
	 */
	String username;
	WebView wv;

	/**
	 * Call {@link Util#findViewById(Activity, ViewGroup, Map)} to map XML item
	 * to View. Call {@link #tabHostSetup()} to link up TabHost and create new
	 * survey record
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_survey);
		uid = getIntent().getIntExtra("uid", -1);
		username = getIntent().getStringExtra("username");
		Tools.findViewById(this, (ViewGroup) findViewById(android.R.id.content), view);
		tabHostSetup();
		surveryReset();
	}

	/**
	 * Setup webview, tabspec and link up tabs
	 */
	private void tabHostSetup() {
		String[] spintext = { "Pie Chart", "Bar Chart" };
		ArrayAdapter<String> aa = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, spintext);
		aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		((Spinner) view.get("spChart")).setAdapter(aa);
		((Spinner) view.get("spChart")).setSelection(0);
		TabHost tabHost = ((TabHost) view.get("tabhost"));
		tabHost.setup();
		TabSpec tab1 = tabHost.newTabSpec("Survey");
		TabSpec tab2 = tabHost.newTabSpec("List");
		TabSpec tab3 = tabHost.newTabSpec("Statistics");
		tab1.setIndicator("Survey");
		tab2.setIndicator("List");
		tab3.setIndicator("Statistics");
		tab1.setContent(R.id.tab1);
		tab2.setContent(R.id.tab2);
		tab3.setContent(R.id.tab3);
		tabHost.addTab(tab1);
		tabHost.addTab(tab2);
		tabHost.addTab(tab3);
		TabWidget tabHeader = (TabWidget) findViewById(android.R.id.tabs);
		for (int i = 0; i < tabHeader.getChildCount(); i++) {
			ViewGroup vg = (ViewGroup) tabHeader.getChildAt(i);
			TextView title = ((TextView) vg.findViewById(android.R.id.title));
			System.out.println(title.getText().toString());
			title.setTextSize(10);
		}

		wv = ((WebView) view.get("wvChart"));
		wv.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {
				view.setBackgroundColor(0x00000000);
				if (Build.VERSION.SDK_INT >= 11)
					view.setLayerType(WebView.LAYER_TYPE_SOFTWARE, null);
			}
		});
		WebSettings ws = wv.getSettings();
		ws.setSupportMultipleWindows(true);
		ws.setJavaScriptEnabled(true);
		wv.loadUrl("about:blank");
	}

	/**
	 * Reset all view's value to default and get next survey ID
	 */
	private void surveryReset() {
		((SeekBar) view.get("sbAgeRange")).setProgress(0);
		((RadioGroup) view.get("rgGender")).clearCheck();
		((RatingBar) view.get("rbRating")).setRating(0.0f);
		((TextView) view.get("tvUserName")).setText("Login User: " + username);
		getSurveyId();
	}

	/**
	 * Get next survey ID
	 */
	private void getSurveyId() {
		DbHelper db = DbHelper.getInstance(getApplicationContext());
		Cursor c = db.rawQuery("select seq from sqlite_sequence where name='Surveys'", null);
		int sid = 0;
		if (c.moveToFirst()) {
			sid = c.getInt(0);
		}
		if (sid > 0) {
			((TextView) view.get("tvSurveyId")).setText("" + (sid + 1));
		} else {
			((TextView) view.get("tvSurveyId")).setText("1");
		}
	}

	/**
	 * Insert survey data to database
	 */
	private void insertSurvey() {
		AgeRange agerange = AgeRange.values()[((SeekBar) view.get("sbAgeRange")).getProgress()];
		Gender gender = Gender.values()[Integer
				.parseInt(((RadioButton) findViewById(((RadioGroup) view.get("rgGender"))
						.getCheckedRadioButtonId())).getTag().toString())];
		Rating rating = Rating.values()[Math.round(((RatingBar) view.get("rbRating")).getRating())];
		long createDate = System.currentTimeMillis();

		DbHelper db = DbHelper.getInstance(getApplicationContext());
		Survey survey = new Survey(uid, agerange, gender, rating, createDate);
		if (db.insertSurvey(survey) > -1) {
			surveryReset();
			Toast.makeText(this, "Record inserted", Toast.LENGTH_LONG).show();
		} else {
			Toast.makeText(this, "Error", Toast.LENGTH_LONG).show();

		}

	}

	/**
	 * Check survey data complete or not
	 * 
	 * @return boolean Return false when not all have data
	 */
	private boolean checkData() {
		boolean result = false;
		if (!(((SeekBar) view.get("sbAgeRange")).getProgress() == 0
				| ((RadioGroup) view.get("rgGender")).getCheckedRadioButtonId() == -1 | ((RatingBar) view
					.get("rbRating")).getRating() == 0)) {
			result = true;
		}
		return result;
	}

	/**
	 * List survey statistics base on field
	 */
	private void listSurveyStatistics() {
		ArrayList<HashMap<String, String>> list;
		String field = " ";
		String group = " ";
		String title = "No.      ";
		if (((CheckBox) view.get("cbAgeRange")).isChecked()) {
			field += "agerange as '" + Constant.SURVEY_LIST_HEADER[1] + "',";
			group += "agerange,";
			title += "Age      ";
		}

		if (((CheckBox) view.get("cbGender")).isChecked()) {
			field += "gender as '" + Constant.SURVEY_LIST_HEADER[2] + "',";
			group += "gender,";
			title += "Gender  ";
		}

		if (((CheckBox) view.get("cbRating")).isChecked()) {
			field += "rating as '" + Constant.SURVEY_LIST_HEADER[3] + "',";
			group += "rating,";
			title += "Rating ";
		}

		field = field.substring(0, field.length() - 1);
		group = group.substring(0, group.length() - 1);
		String sql = "SELECT count(*), " + field + "  from surveys group by " + group;

		if (((CheckBox) view.get("cbAgeRange")).isChecked()
				|| ((CheckBox) view.get("cbGender")).isChecked()
				|| ((CheckBox) view.get("cbRating")).isChecked()) {

			DbHelper db = DbHelper.getInstance(getApplicationContext());
			Cursor cursor = db.rawQuery(sql, null);
			list = new ArrayList<HashMap<String, String>>();

			while (cursor.moveToNext()) {
				LinkedHashMap<String, String> temp = new LinkedHashMap<String, String>();
				for (int i = 0; i < cursor.getColumnCount(); i++) {
					if (cursor.getColumnName(i).equals("Age Range")) {
						temp.put(cursor.getColumnName(i),
								AgeRange.values()[cursor.getInt(i)].toString());
					} else if (cursor.getColumnName(i).equals("Gender")) {
						temp.put(cursor.getColumnName(i), Gender.values()[cursor.getInt(i)]
								.toString().substring(0, 1));
					} else {
						temp.put(cursor.getColumnName(i), cursor.getString(i));
					}
				}
				list.add(temp);
			}
			ListSurveyAdapter adapter = new ListSurveyAdapter(this, list);
			((ListView) view.get("lvGroupBy")).setAdapter(adapter);
			// ((TextView) view.get("tvGroupBy00")).setText(sql);
			((TextView) view.get("tvGroupByTitle")).setText(title);
			ShowChart(list);
			DbHelper.close(cursor);
		} else {
			// Toast.makeText(this,
			// "Please select at least one field for grouping",
			// Toast.LENGTH_SHORT).show();
			((TextView) view.get("tvGroupByTitle")).setText("");
			((ListView) view.get("lvGroupBy")).setAdapter(null);
			ShowChart(new ArrayList<HashMap<String, String>>());
		}

	}

	/**
	 * switch VIEW of chart area
	 */
	private void switchView(View v) {
		ViewGroup vg = ((ViewGroup) view.get("tabChart"));
		vg.removeAllViews();
		vg.addView(v, 0);
	}

	/**
	 * Show chart according to selection
	 */
	private void ShowChart(ArrayList<HashMap<String, String>> list) {
		if (((Spinner) view.get("spChart")).getSelectedItemPosition() == 0) {
			PieChart(PieChartData(list));
		} else {
			switchView(new BarChart(getApplicationContext(), list));
		}
	}

	/**
	 * Draw pie chart to webview with Google chart tools
	 * 
	 * @param data
	 */
	private void PieChart(String data) {
		//@formatter:off
		String content_h =
		 "<html>"
		+"  <head>"
		+"   <script type=\"text/javascript\" src=\"jsapi.js\"></script>"
		+"    <script type=\"text/javascript\">"
		+"    google.load('visualization', '1.0', {'packages':['corechart']});"
		+"    google.setOnLoadCallback(drawChart);"
		+"    function drawChart() {";
/*		+"    var data = new google.visualization.DataTable();	"
		+"	  data.addColumn('string', 'Gender');"
		+"    data.addColumn('number', 'Count');"
		+"    data.addColumn('string', 'Age Range');"
		+"	  data.addColumn('number', 'Rating');"
		+"    data.addRows(["
		+"    ['Female Rating 4 Age 11-20',3, '11-20', 4],"
		+"    ['Male Rating 2 Age 11-20',5, '11-20', 2],"
		+"    ['Female Rating 4 Age 21-30',2, '21-30', 4],"
		+"    ['Male Rating 1 Age 21-30', 4,'21-30', 1],"
		+"    ['Female Rating 5 Age 31-40', 2,'31-40', 5]"
		+"    ]);"
		+"    var options = {'title':'How Much Pizza I Ate Last Night',"*/
		String content_f =
		 "                   'width':320,"
		+"                   'height':250,"
		+"				     backgroundColor: { fill:'transparent' }"
		+"				    };"
		+"    var chart = new google.visualization.PieChart(document.getElementById('chart_div'));"
		+"    chart.draw(data, options);"
		+"    }"
		+" </script>"
		+"</head>"
		+"<body>"
		+" <div id=\"chart_div\"></div>"
		+" </body>"
		+"</html>";
		//@formatter:on
		data = content_h + data + content_f;
		System.out.println(data);
		switchView(wv);
		wv.loadDataWithBaseURL("file:///android_asset/", data, "text/html", "utf-8", null);

	}

	/**
	 * Inject chart data into Google chart tools
	 * 
	 * @param list
	 * @return String
	 */
	private String PieChartData(ArrayList<HashMap<String, String>> list) {
		String data, gender, rating, agerange, title;
		title = "  ";
		data = "";
		data += "var data = new google.visualization.DataTable();";
		data += "data.addColumn('string', 'Title');";
		data += "data.addColumn('number', 'Count');";
		data += "data.addRows([ ";
		for (HashMap<String, String> m : list) {
			title = "  ";
			data += "['";
			if (m.get("Gender") != null) {
				if (m.get("Gender").equals("F")) {
					gender = "Female ";
				} else {
					gender = "Male ";
				}
				title += "Gender, ";
			} else {
				gender = "";
			}
			data += gender;
			if (m.get("Rating") != null) {
				rating = "Rating " + m.get("Rating") + " ";
				title += "Rating, ";
			} else {
				rating = "";
			}
			data += rating;
			if (m.get("Age Range") != null) {
				agerange = "Age " + m.get("Age Range") + " ";
				title += "Age Range, ";
			} else {
				agerange = "";
			}
			data += agerange;
			data += "', " + Integer.parseInt(m.get("count(*)")) + "],";
		}
		data = data.substring(0, data.length() - 1) + "]);";
		data += "var options = {'title':'Survey statistics grouped by "
				+ title.substring(0, title.length() - 2) + "',";

		return data;
	}

	/**
	 * List survey data to ListView
	 */
	private void listSurveyData() {
		ArrayList<HashMap<String, String>> list;
		DbHelper db = DbHelper.getInstance(getApplicationContext());
		Cursor cursor = db
				.rawQuery(
						"select s._id,agerange,gender,rating,username,createdate from Surveys s, Users u on s.uid = u._id ",
						null);
		list = new ArrayList<HashMap<String, String>>();
		while (cursor.moveToNext()) {
			String sid = cursor.getString(0);
			int agerange = cursor.getInt(1);
			int gender = cursor.getInt(2);
			String rating = cursor.getString(3);
			String username = cursor.getString(4);
			Date createDate = new Date(cursor.getLong(5));
			LinkedHashMap<String, String> temp = new LinkedHashMap<String, String>();
			temp.put(Constant.SURVEY_LIST_HEADER[0], sid);
			temp.put(Constant.SURVEY_LIST_HEADER[1], AgeRange.values()[agerange].toString());
			temp.put(Constant.SURVEY_LIST_HEADER[2],
					Gender.values()[gender].toString().substring(0, 1));
			temp.put(Constant.SURVEY_LIST_HEADER[3], rating);
			temp.put(Constant.SURVEY_LIST_HEADER[4], username);
			temp.put(Constant.SURVEY_LIST_HEADER[5], createDate.toLocaleString());
			list.add(temp);
		}
		ListSurveyAdapter adapter = new ListSurveyAdapter(this, list);
		((ListView) view.get("lvList")).setAdapter(adapter);
		DbHelper.close(cursor);
	}

	/**
	 * Clean and restore all table from CSV file
	 * 
	 * @throws IOException
	 */
	private void restoreData() throws IOException {
		BufferedReader buffer = null;
		try {
			FileReader file = new FileReader(Constant.CSV_PATH);
			buffer = new BufferedReader(file);
			ArrayList<String> sqls = new ArrayList<String>();
			String line = "";
			String sql = "";
			String sql_surveys = "INSERT INTO surveys(_id,uid,agerange,gender,rating,createdate) values(";
			String sql_users = "INSERT INTO users(_id,username,password) values(";
			sqls.add("delete from surveys");
			sqls.add("delete from users");
			while ((line = buffer.readLine()) != null) {
				if (line.contains("Table") || line.toLowerCase(Locale.US).contains("id")) {
					if (line.contains("[Surveys Table]")) {
						sql = sql_surveys;
					}
					if (line.contains("[Users Table]")) {
						sql = sql_users;
					}
					continue;
				}
				String[] data = line.split(",");
				StringBuffer sb = new StringBuffer(sql);
				for (String col : data) {
					if ((col.startsWith("\"") && col.endsWith("\""))
							|| (col.startsWith("'") && col.endsWith("'"))) {
						sb.append(col).append(",");
					} else {
						sb.append("'").append(col).append("'").append(",");
					}
				}
				sb.delete(sb.length() - 1, sb.length()).append(");");
				sqls.add(sb.toString());
			}
			DbHelper db = DbHelper.getInstance(getApplicationContext());
			db.querysWithTransaction(sqls);
		}catch (FileNotFoundException e){
			Toast.makeText(getApplicationContext(), "File not found: /SdCard/Download/vtc.android.assignment.csv", Toast.LENGTH_LONG).show();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (buffer != null)
				buffer.close();
		}

	}

	/**
	 * Call {@link #writeCsv(ArrayList, boolean)} to save record to CSV file
	 * 
	 * @throws IOException
	 */
	private void exportData() throws IOException {
		DbHelper db = DbHelper.getInstance(getApplicationContext());
		writeCsv(new String[] { "[Surveys Table]" }, false);
		writeCsv(new String[] { "Survey ID", "Age Range", "Gender", "Rating", "uid", "Date" }, true);
		writeCsv(db.dbToArrayList("Surveys"), true);
		writeCsv(new String[] { "[Users Table]" }, true);
		writeCsv(new String[] { "User ID", "User Name", "Password" }, true);
		writeCsv(db.dbToArrayList("users"), true);
		Toast.makeText(this, "Record exported to:" + Constant.CSV_PATH, Toast.LENGTH_LONG).show();
	}

	/**
	 * Overload method of {@link #writeCsv(ArrayList, boolean)}
	 * 
	 * @param strings Array of string to write to csv
	 * @param append For FileWriter append mode
	 * @throws IOException
	 */
	private void writeCsv(String[] strings, boolean append) throws IOException {
		ArrayList<String[]> arrayList = new ArrayList<String[]>();
		arrayList.add(strings);
		writeCsv(arrayList, append);
	}

	/**
	 * Write ArrayList to CSV file
	 * 
	 * @param data The ArrayList to write to file
	 * @param append FileWrite write mode
	 * @throws IOException
	 */
	private void writeCsv(ArrayList<String[]> data, boolean append) throws IOException {
		CSVWriter writer = null;
		try {
			writer = new CSVWriter(new FileWriter(Constant.CSV_PATH, append), ',');
			for (String[] row : data) {
				writer.writeNext(row);
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (writer != null)
				writer.close();
		}
	}

	/**
	 * Show confirmation and clean all survey data
	 */
	private void cleanSurvey() {
		AlertDialog dialog = new AlertDialog.Builder(this).create();
		dialog.setTitle("Alert:");
		dialog.setMessage("Are you sure want to delete all survey record?");
		dialog.setCancelable(false);
		dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int buttonId) {
						int result = 0;
						DbHelper db = DbHelper.getInstance(getApplicationContext());
						result = db.deleteAllSurvey();
						Toast.makeText(SurveyActivity.this,
								"Survey data cleaned, total:" + result + " record",
								Toast.LENGTH_LONG).show();
						listSurveyData();
						listSurveyStatistics();
						getSurveyId();
					}
				});
		dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int buttonId) {

					}
				});
		dialog.setIcon(R.drawable.icon);
		dialog.show();
	}

	/**
	 * Handle all click event Submit survey record
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btSubmitSurvey:
			if (checkData()) {
				insertSurvey();
			} else {
				Toast.makeText(this, "Data not complete", Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.cbAgeRange:
		case R.id.cbGender:
		case R.id.cbRating:
			listSurveyStatistics();
			break;

		}

	}

	/**
	 * Handle SeekBar event and update textview of progress
	 */
	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
		((TextView) view.get("tvShowAge")).setText(AgeRange.values()[progress].toString());

	}

	/**
	 * Handle TabHost Event List all record of survey data, statistics and chart
	 */
	@Override
	public void onTabChanged(String tabId) {
		switch (tabId) {
		case "Suvery":
			break;
		case "List":
			listSurveyData();
			break;
		case "Statistics":
			listSurveyStatistics();
			break;
		}
	}

	/**
	 * Link up menu item
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.survey, menu);
		return true;
	}

	/**
	 * Handle menu item's action
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_logout:
			startActivity(new Intent(this, MainActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
			break;
		case R.id.action_addUser:
			startActivity(new Intent(this, CreateUserActivity.class));
			break;
		case R.id.action_changePass:
			Intent i = new Intent(this, ChangePassActivity.class);
			i.putExtra("uid", uid);
			i.putExtra("username", username);
			startActivity(i);
			break;
		case R.id.action_cleanSurvey:
			cleanSurvey();
			break;
		case R.id.action_restore:
			try {
				restoreData();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			break;
		case R.id.action_export:
			try {
				exportData();
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		listSurveyStatistics();

	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		// TODO Auto-generated method stub

	}

}
