package vtc.android.assignment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import vtc.android.assignment.model.User;
import vtc.android.assignment.util.DbHelper;
import vtc.android.assignment.util.Tools;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Activity to handle login
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class LoginActivity extends Activity implements OnClickListener {
	/**
	 * Use to hold all View items
	 */
	Map<String, View> view = new HashMap<String, View>();
	/**
	 * User ID of logged in user
	 */
	int uid;
	/**
	 * User name of logged in user
	 */
	String username;

	/**
	 * Call {@link Util#findViewById(Activity, ViewGroup, Map)} to map XML item to View
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		Tools.findViewById(this, (ViewGroup) findViewById(android.R.id.content), view);
	}

	/**
	 * Handling all click event. When Login button clicked, call {@link #checkLogin()} to verify and load {@link SurveyActivity}
	 */
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btLogin:
			if (checkLogin()) {
				Intent i = new Intent(this, SurveyActivity.class);
				i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
				i.putExtra("uid", uid);
				i.putExtra("username", username);
				startActivity(i);
			}
			break;
		}

	}

	/**
	 * Match username and password with record in database
	 * 
	 * @return boolean Return true if username and password match with database record
	 */
	private boolean checkLogin() {
		try {
			DbHelper db = DbHelper.getInstance(getApplicationContext());
			String etUserName = ((EditText) view.get("etUserName")).getText().toString().toLowerCase(Locale.US);
			String etPassword = Tools.md5(((EditText) view.get("etPassword")).getText().toString());
			ArrayList<User> users = db.listUser("username = ?", new String[]{etUserName}, null, null, null);
			if (users.size() > 0) {
				if (users.get(0).getmPassword().equals(etPassword)){
					uid = users.get(0).getmId();
					username = users.get(0).getmUserName();
					return true;
				} else {
					Toast.makeText(this, "Password does not match for user: " + users.get(0).getmUserName(), Toast.LENGTH_LONG).show();
				}
			} else {
				Toast.makeText(this, "User: " + etUserName + " does not exists", Toast.LENGTH_LONG).show();
			}
		} catch (SQLiteException e) {
			e.printStackTrace();
		} 
		return false;

	}
}
