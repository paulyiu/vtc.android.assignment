package vtc.android.assignment;

import java.util.ArrayList;

import vtc.android.assignment.model.User;
import vtc.android.assignment.util.Constant;
import vtc.android.assignment.util.DbHelper;
import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;

/**
 * <h1>The main entry activity of Android Food Express Survey Android Apps</h1>
 * <p>
 * This activity will initial create two TABLE {@link Constant#SQL_DROP_USERS},
 * {@link Constant#SQL_TABLE_SURVEYS} at getFilesDir() + "/SurveyDB" if the
 * database file not found and prompt user to create first user, after that will
 * forward user to login activity.
 * 
 * @author Yiu Nagi Pang (111809880)
 * @version 1.0
 * @since 2015-03-28
 */
public class MainActivity extends Activity {

	/**
	 * Call {@link #checkUserDBExist()} and {@link #initDb()} nitDB or load
	 * {@link LoginActitity}
	 * 
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (checkUserExist()) {
			// load login if users db exist
			startActivity(new Intent(this, LoginActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
		} else {
			// load create user page if no user in db
			startActivity(new Intent(this, CreateUserActivity.class)
					.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
		}

	}

	/**
	 * Check is any user account exist in the database file
	 * 
	 * @return boolean Return true if user account exist, default return false
	 */
	private boolean checkUserExist() {
		try {
			DbHelper db = DbHelper.getInstance(getApplicationContext());
			ArrayList<User> users = db.listUser(null, null, null, null, null);
			return (users.size() > 0) ? true : false;
		} catch (SQLiteException e) {
			e.printStackTrace();
		}
		return false;

	}

}
